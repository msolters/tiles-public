# **Tiles.js** #

## Personal and professional portfolio web software. ##


# **Dependencies** #

1. MongoDB
2. jQuery >= 1.10

### Install ###

1. Clone the repo to your web directory
2. If there are more than once instances of Tiles running on the same machine, give each instance a unique $username inside the file /tiles/user.php.  If there is only one user on this machine, this step can be skipped.
3. cd to the /tiles directory, and execute 
```
#!bash

sudo sh install_mongodb_ubuntu.sh
```

if MongoDB is not already installed.

4. Finally, to configure the backend, set permissions using the following script.  Make sure you execute it from its own directory in the command line!!
```
#!bash

sudo sh set_permissions.sh
```


### Setup ###

As of now, the front and backend should be reachable by pointing your browser towards the domain or directory of the repo clone.  However, the backend is unprotected.  Follow these steps to secure.

1. Access (domain)/tiles/backend in your browser.  (Recommended behaviour is to set ../tiles as the root directory of the virtual machine for this domain, so that /tiles is implied.
2. In the backend Control Panel, change the user name and login to your desired parameters.
3. Hit save.  The page will refresh, prompting you to re-enter the new login parameters.
4. The backend is now secure.

### Managing Content ###

Adding, renaming, and sorting of Categories and Tiles can be done from the /backend after logging in.



This software is based on an idea by Eric Pheterson and created by Mark Solters.
