###
	This class creates and controls an individual session of the Tiles software.  It accepts up to two parameters:
		canvas == The jQuery object of the div that is meant to be the canvas of this instance
		rename page == Rename the page title of the canvas div with the user's display name.  If
							not given, or if not explicitly false, the default will be to leave the
							page title as is.

	Example usage:

	<html>
	<head>
		<link href="css/tiles.css" rel="stylesheet" type="text/css">
	</head>
	
	<body>

		<div id="maincanvas"></div>
	
		<script src="js/tiles.js"></script>
		
		<script type="text/javascript" language="javascript">
			// this example will rename the page with the user's display name
			var mainTilepen = new Tilepen($("#maincanvas"), true);
		</script>
	
	</body>
	</html>


	There are two main views for a Tiles canvas, vertical or horizontal.
	Crossing from one to the other will unfortunately restart any iFrames playing content.
###

class Tilepen
	w_min:		460			#	Minimum width and height determine the size of the window where vertical mode will start.
	h_min:		400			#	Note:  These must match media query in css/tiles.css!
	x_offset: 	20			
	y_offset: 	80
	sort_type:	"category"	#	Default sort type
	canvas_map:	{}			#	What categories belong in which categories
	tiles:		{}			#	All loaded tiles (non-destructive)
	categories:	{}			#	All categories, and their associated info for quick reference
	pending:	0
	pendingmax: 0
	quotes:		[ "Put", "quotes", "here" ]
	
	constructor: (@canvas, @title_override = false) ->
#	Initialize parameters describing canvas geometry
		@w = @canvas.width()
		@h = @canvas.height()
		@viewSwitched()
		@canvas.css {"position": "relative"}
		@canvas.append '<div class="tilescroll"><div class="tilepen"></div></div>'
		@tilescroll = @canvas.find ".tilescroll"
		@tilepen = @tilescroll.find ".tilepen"
		@overlay = new Overlay @
		@tilescroll.mousewheel (e, delta) =>
			if @view is "horizontal"
				@tilescroll.scrollLeft((@tilescroll.scrollLeft()-(30*delta)))
				e.preventDefault()
#	This function handles any click on the canvas
		@tilescroll.click (e) =>
			elem = e.target
			if @overlay.visible
				if !$(elem).is ".overlay *, .overlay"
#	If the overlay is open, and the user clicked something else,
#	close the overlay.
					@overlay.hide()
			else
#	If the overlay's visible property is not yet == true,
#	but a tile was clicked, set the visible property to true.
				if $(elem).is(".tile *, .tile")
					if @overlay.v_trig is true
						@overlay.visible = true
						@overlay.v_trig = false
			@toggleDropdown("hide")
#	Set resize callbacks to keep canvas geometry consistent
		$(window).resize =>
			@canvasResized()
			@renderCanvas( @viewSwitched() ) if @tilescroll.is(":visible") 
		@tilescroll.scroll => @getColor()
#	Finally, populate the canvas with user data
#		@getQuoteofDay()
		@getDisplayName()
		
	getQuoteofDay: =>
		@q_size = @quotes.length# of quotes
		dt_now = new Date()
		TZ_offset = dt_now.getTimezoneOffset() * 60000
		now = dt_now.getTime() - TZ_offset
		day = Math.floor(now/3600000)
		@quote = day % @q_size
		@qod = @quotes[@quote]
		
#	getDisplayName() gets the user's name from the DB and stores it in @dname
	getDisplayName: =>
		$.ajax 'php/get-dname.php',
			type: 'GET'
			error: (jqXHR, textStatus, errorThrown) ->
				alert "AJAX Error: #{textStatus}\n#{errorThrown}"
			success: (data, textStatus, jqXHR) =>
				@dname = data
				@renderToolbar()
				document.title = @dname if @title_override is true
#				console.log "User's name is #{@dname}"
				
				
#	renderToolbar() does exactly what it sounds like it would do.
	renderToolbar: =>
		toolbar_html = '<div class="top-menu animate">
							<input class="search-input animate" placeholder="Search" type="text">
								<img class="search-input-icon" src="img/search4.png"></i>
							</input>
							<div class="top-menu-spanner">
								<i class="fa fa-fw fa-chevron-down spanner-icon"></i>
							</div>
							<div class="top-menu-name animate">'+@dname+'</div>
							<div class="top-menu-dropdown">
								<div class="top-menu-dropdown-inner"></div>
							</div>
						</div>'
		@canvas.append toolbar_html
		@dropdown = @canvas.find ".top-menu-dropdown"
		@dropdowninner = @canvas.find ".top-menu-dropdown-inner"
		@topmenuspanner = @canvas.find ".top-menu-spanner"
		@spannericon = @topmenuspanner.find ".spanner-icon"
		@searchinput = @canvas.find ".search-input"
		@searchinputicon = @canvas.find ".search-input-icon"
		@topmenuname = @canvas.find ".top-menu-name"
		@topmenu = @canvas.find ".top-menu"
		@topmenuname.css {"left": ("calc(50% - " + (@topmenuname.width()/2) + "px)") }
		
		@searchinput.hover => @expandToolbar()
		@searchinputicon.hover => @expandToolbar()
		@searchinput.focus => @expandToolbar()
		@searchinput.focusout => @leaveToolbar()
		@searchinput.mouseleave =>
			@leaveToolbar() if !@searchinput.is ":focus"
		@searchinputicon.mouseleave =>
			@leaveToolbar() if !@searchinput.is ":focus"

		@searchinputicon.click => @clearSearch()
		@q_Interval = 400
		@searchinput.keyup =>
			if @qTimer
				clearTimeout @qTimer
			@qTimer = setTimeout =>
				@getCanvasData()
			, @q_Interval
		@topmenuspanner.click => @toggleDropdown()
#	If this is there is no canvas data loaded yet, get some		
		@getCategories()
		
	toggleDropdown: (action="none") =>
		switch action
			when "show"
				@dropdowninner.slideDown()
				@spannericon.removeClass "fa-chevron-down"
				@spannericon.addClass "fa-chevron-up"
			when "hide"
				@dropdowninner.slideUp()
				@spannericon.removeClass "fa-chevron-up"
				@spannericon.addClass "fa-chevron-down"
			else
				@dropdowninner.slideToggle()
				if @spannericon.hasClass "fa-chevron-down"
					@spannericon.removeClass "fa-chevron-down"
					@spannericon.addClass "fa-chevron-up"
				else
					@spannericon.removeClass "fa-chevron-up"
					@spannericon.addClass "fa-chevron-down"
		
	chooseCategory: (category_choice) =>
		@spannericon.removeClass "fa-chevron-up"
		@spannericon.addClass "fa-spinner"
		@dropdowninner.hide()
		category = category_choice.html()
		l = @categories[category].left
		r = @categories[category].right
		dx = l
		if @view is "horizontal"
			dx -= 10
			if (l + @w) > @tilepen.width() or (r - dx) < ((@w * dx)/(@tilepen.width()-@w) )
				dx = l/( 1 + ( (@w + l - r)/(@tilepen.width()-@w) ) )
		#alert dx
		@tilescroll.stop().animate({ scrollLeft: dx }, 1000)
		@spannericon.removeClass "fa-spinner"
		@spannericon.addClass "fa-chevron-down"
		
	getCategories: =>
		$.ajax 'php/get-categories.php',
			type: 'GET'
			error: (jqXHR, textStatus, errorThrown) ->
				console.log "AJAX Error, in findTiles(): #{textStatus}\n#{errorThrown}"
				return false
			success: (data, textStatus, jqXHR) =>
				menu_html = '<div class="sort-toggle-choice"><div class="sort-text">Sort by: </div><div class="sort-type">Date</div></div>'
				for category, colour of data.category_colours
					@categories[category] = new Category(category, colour) if not @categories[category]
					menu_html += '<div class="category-choice" style="border-color: '+colour+'">'+category+'</div>'
				@canvas.find('.top-menu-dropdown-inner').html(menu_html)
				@canvas.find(".category-choice").each (index, element) =>
					$(element).click =>
						@chooseCategory( $(element) )
				@sorttogglechoice = @canvas.find(".sort-toggle-choice")
				@sorttypetext = @canvas.find(".sort-type")
				@sorttogglechoice.click => @toggleSortMode()
				@getCanvasData() if !@canvas_data
				return true
				
	getColor: =>
		if @sort_type is "date"
			return
		r = (@tilescroll.scrollLeft() / (@tilepen.width() - @tilescroll.width()))
		x = @x_offset + (r*@w) + @tilescroll.scrollLeft()
		for label, category of @categories
			if x >= category.left and x <= category.right
				@dropdown.css {"border-color": category.colour}

		
	expandToolbar: =>
#		console.log "Expand toolbar!"
		@searchinput.addClass "expanded"
		input_length = @topmenu.width() - 80
		@topmenuname.hide()
		@searchinput.css {"width": ((input_length - 55)+"px"), "max-width": ((input_length - 55)+"px")}
		@topmenuspanner.css {"width": ((@topmenu.width() - input_length)+"px"), "left": (input_length+"px") }

	leaveToolbar: =>
#		console.log "Reduce toolbar!"
		Q = @searchinput.val()
		if Q.length == 0
			@searchinput.removeClass "expanded"
			@topmenuname.removeClass "expanded"
			@searchinput.css {"width": ""}
			@topmenuspanner.css {"width": "", "left": ""}
			@topmenuname.show()
		
	clearSearch: =>
		Q = @searchinput.val()
		if Q.length > 0
			@searchinput.val("")
			@searchinputicon.removeClass("expanded")
			@searchinputicon.attr("src", "img/search4.png")
		else
			@expandToolbar()
		@getCanvasData()
		@searchinput.focus()
	
	toggleSortMode: =>
		switch @sort_type
			when "date"
				@sort_type = "category"
				@sorttypetext.html("Date")
			when "category"
				@sort_type = "date"
				@sorttypetext.html("Category")
				@dropdown.css {"border-color": "#001325"}
		@toggleDropdown("hide")
		@getCanvasData()

#	viewSwitched() returns true if the canvas has resized to the point where
#		it either must go from vertical view to horizontal view, or vice
#		versa.  It updates the @view parameter of its Tilepen object with the
#		newly computed view type.
	viewSwitched: =>
		changed = false
		if @w < @w_min or @h < @h_min
			view_ = "vertical"
		else
			view_ = "horizontal"
		if @view != view_
			console.log "Canvas view switched from #{@view} to #{view_}."
			@view = view_
			changed = true
		return changed

#	canvasResized() checks to see if the target canvas has changed its size,
#		and returns true or false accordingly.  It also updates the @w and @h
#		parameters for its Tilepen object with the new canvas width and height.
	canvasResized: =>
		changed = false
		w = @canvas.width()
		h = @canvas.height()
		if w != @w or h != @h
			@w = w
			@h = h
			changed = true
#			console.log "Canvas changed size."
		return changed

#	Initiates a search
	getCanvasData: =>
		Q = @searchinput.val().trim()
		if Q.length > 0
			@searchinputicon.attr("src", "img/delete13.png")
			@searchinputicon.addClass("expanded")
			@searchinput.css("background", "#fff")
		else
			@searchinputicon.attr("src", "img/search4.png");
			@searchinputicon.removeClass("expanded");
			@searchinput.css("background", "");
		@findTiles(Q)

#	Executes a search
	findTiles: (Q) =>
		@searching = true
		$.ajax 'php/find-tiles.php',
			type: 'GET'
			data: { query: Q, type: @sort_type }
			dataType: 'json'
			error: (jqXHR, textStatus, errorThrown) ->
				console.log "AJAX Error, in findTiles(): #{textStatus}\n#{errorThrown}"
				return false
			success: (data, textStatus, jqXHR) =>
#				console.log "Successful AJAX request: #{JSON.stringify(data)}"
				switch @updateCanvas data.canvas_data
					when true #	we need to get tiles and render
		#				@tilescroll.hide()
						@overlay.overlay_loading.show()
#						@overlay.loading_text.html @getQuoteofDay()#"Gathering information..."
						tiles_needed = []
						if data.tID_list.length > 0
							@overlay.overlay_noresults.hide()
							for tID in data.tID_list
								tiles_needed.push(tID) if not @tiles[tID]
							if tiles_needed.length > 0
#								@overlay.loading_text.html "Processing multimedia..."
								$.ajax 'php/get-tiles.php',
									type: 'GET'
									data: {tID_list: tiles_needed}
									dataType: 'json'
									error: (jqXHR, textStatus, errorThrown) ->
										console.log "AJAX Error, in getTiles(): #{textStatus}\n#{errorThrown}"
										return false
									success: (data, textStatus, jqXHR) =>
						#				console.log "Successful AJAX request: #{JSON.stringify(data)}"
										for tile in data.tiles
											@tiles[tile.tID] = new Tile(tile, @)
							else
								@renderCanvas(true)
						else	# no results!
							@tilescroll.hide()
							@overlay.overlay_loading.hide()
							@overlay.overlay_noresults.show()
							@searching = false
					when false	# the same exact data, skip to rendering
						@renderCanvas(false)

#	updateCanvas() takes one parameter, the JSON object of incoming canvas data.
#		It creates a @canvas_map, describing what categories were returned, their
#		order, and the tiles that belong to each category.  It also generates
#		Tile objects if they don't already exist in the document, which will 
#		inject tile HTML into the body for quick exposure later.
#	Returns:
#		True:  Canvas is different from last query
#		False:	Canvas is the same, no need to add/hide tiles	
	updateCanvas: (canvas_new) =>
		if JSON.stringify(canvas_new) is JSON.stringify(@canvas_data)
			return false
		@canvas_map = {}	#	Represents the desired order of categories and tiles
		for category, tiles of canvas_new
			tID_list = []
			for tID in tiles
				tID_list.push(tID)
			@canvas_map[category] = tID_list
#		console.log "New canvas map: #{JSON.stringify(@canvas_map)}"
		@canvas_data = canvas_new
		return true

	processSoundcloud: (target) =>
		o = if target.hasClass("tile") then false else true
		n = 0
		target.find(".soundcloud").each (index, element) =>
			if !o
				@pending += 1
				@pendingmax += 1
#			@overlay.loading_text.html "Processing multimedia "+@pending
			n = index
			track = $(element).attr("track")
			height = $(element).attr("height")
			width = $(element).attr("width")
			player_html = '<iframe width="'+width+'" height="'+height+'" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url='+track+'&amp;auto_play=true&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>'
			$.getJSON "http://soundcloud.com/oembed", {url: track, format: "json"}, (data) =>
				thumb_url = data.thumbnail_url
				title = data.title
				thumb_html = '<img src="'+thumb_url+'" class="sc-img" style="max-height: '+height+'; height: '+height+'; max-width: '+width+';" ><div class="sc-play"><div class="sc-title">'+title+'</div><i class="fa fa-fw fa-3x fa-play-circle-o"></i><div class="sc-load">Click to load</div></div>'
				$(element).html(thumb_html)
				$(element).find("img").click =>
					$(element).html player_html
				if !o
					@pending -= 1
#					@overlay.loading_text.html "Processing multimedia: "+(@pendingmax-@pending)+"/"+@pendingmax
					@checkCanvasReady()
				else
					@overlay.show()
					return
		@overlay.show() if o
				
	checkCanvasReady: =>
		if @pending is 0
			Prism.highlightAll()
#			@overlay.loading_text.html "Waiting for images to load..."
			@cimg = @canvas.imagesLoaded()
			@cimg.always =>
				@pendingmax = 0 
				@renderCanvas true
#			@cimg.progress =>
				
		
#	renderCanvas() will display the tiles in either vertical or horizontal 
#		layout. Passing the optional parameter redraw=true will force all
#		tiles to be reloaded.  This is used when search results change,
#		for instance.
	renderCanvas: (redraw=false) =>
		c = 1
		x = 0
		@tilescroll.show()
		switch @view
			when "vertical"
				x = 0
				@canvas.find(".tile").hide() if redraw
			when "horizontal"
				x = @x_offset
				@canvas.find(".tile").hide()
				@tilepen.find(".next-column-btn").hide()
		@tilepen.find(".column").hide() if redraw
		@tilepen.find(".column-header").remove()
		@dropdown.find(".category-choice").hide()
		for category, tIDs of @canvas_map
#			console.log "=== New Category ==="
			if $('.column-'+c).length == 0
				@tilepen.append '<div class="column column-'+c+'"></div>'
			column_ = @tilepen.find '.column-'+c
			
			if $('.column-header-'+c).length == 0
				column_.append '<div class="column-header column-header-'+c+'"></div>'
			column_header_ = $('.column-header-'+c)
			
			if @sort_type is "category"
				column_header_.html category
				column_header_.css {"color": @categories[category].colour, "font-size": "28px"}
				@canvas.find(".category-choice").each (index, element) =>
					$(element).show() if category == $(element).html()
			else
				column_header_.html ""
				
			y = @y_offset
			k = 1
			t = 0
			
			switch @view
				when "vertical"
					if redraw
						@tilepen.find(".tile").css {"left": "", "top": ""}
						@tilepen.find(".column").css {"max-width": "", "width": ""}
						for tID in tIDs
							column_.append @tiles[tID].t
							@tiles[tID].grab()
							@tiles[tID].show()
#							@tiles[tID].t.find("a").click (e) ->
#								e.stopPropagation()
					column_.css "left", ((x)+"px")
					if @sort_type is "category"
						@categories[category].left = x
						@categories[category].right = x + column_.width()
					x+=column_.outerWidth()
				when "horizontal"
					x_0 = x
					column_header_.css {"top": ((@y_offset-23)+"px")}
					for tID in tIDs
						@tilepen.append @tiles[tID].t if redraw
						@tilepen.find(".column").css {"max-width": "none"} if redraw
						@tiles[tID].show()
						y_delta = @tiles[tID].t.outerHeight() + 15
						if ((10 + y + y_delta) > @h) and (t != 0)
#							console.log "new column!"
							k++
							y = @y_offset
							x += 440
						if y is @y_offset and @sort_type is "date"
							@tilepen.append '<div class="column-header column-header-'+c+'-'+k+'">'+@tiles[tID].end_date+'</div>'
							date_column_header_ = @tilepen.find '.column-header-'+c+'-'+k
							date_column_header_.css { "left": (x+"px")}
#						console.log "Category #"+c+", Tile #"+t+"; row: "+k+"; (x: "+x+", y: "+y+")"
						@tiles[tID].t.css {"left": (x+"px"), "top": (y+"px") }
						y += y_delta
						t++
					column_.css {"width": (( k * 440 ) + "px"), "left": (x_0+"px")}
					if @sort_type is "category"
						@categories[category].left = x_0
						@categories[category].right = x_0 + (k * 440)
					x += 460
			column_.show() if tIDs.length > 0
			c++
		$(".column-header").css { "top": ((@y_offset-23)+"px") }
		@tilepen.css {"width": ((x)+"px")}
		if @view is "vertical"
			@tilepen.find(".column").css {"max-width": ((@tilescroll.width() - 20)+"px")} if @w < @w_min
			@tilescroll.stop().animate({ scrollLeft: 0 }, 1000)
		@tilepen.find(".tile").each (index, element) =>
			if ($(element).find(".tile-content").outerHeight() + $(element).find(".tile-header").outerHeight() + $(element).find(".tile-date").outerHeight()) > ($(element).height())
				$(element).find(".fade-overlay").show()
			else
				$(element).find(".fade-overlay").hide()
		@getColor() if @sort_type is "category"
		@overlay.overlay_loading.hide()
					
class Category
	constructor: (@label, @colour) ->

###
	This class is used to handle individual tiles through a JS object.
	Methods:
		- Constructor inserts the HTML from an individual tile's JSON
		- Hide will hide the HTML object using jQuery
		- Show will show the HTML using jQuery
###
class Tile
	constructor: (tile, @canvas) ->
		@tID = tile.tID
		@title = tile.title
		@content = tile.content
		@content_s = tile["content-s"]
		@content_l = tile["content-l"]
		@color = tile.color
		@date = tile.date
		@start_date = tile["start-date"]
		@end_date = tile["end-date"]
		@fuzzy_date = tile["fuzzy-date"]
		tile_html =	'<div id="'+tile.tID+'" class="tile" style="border-color: '+@color+'">
						<div class="fade-overlay" style="background-image: linear-gradient(to top, '+@color+', '+@color+' 6%, transparent 8%)">
							<div class="fade-overlay-text">...</div>
						</div>
						<div class="tile-bounding-box">
						<div class="row tile-header">
							<div class="tile-title">'+@title+'</div>
						</div>
						<div class="row tile-date">
							<div class="start-date">'+@start_date+'</div>'
						
		tile_html += ' - ' if (@start_date.length > 0) and (@end_date.length > 0)
		
		tile_html += '<div class="end-date">'+@end_date+'</div>
						</div>
						<div class="row tile-content">'
		if @content_s.length > 0
			tile_html += @content_s
		else
			tile_html += @content
		tile_html += '</div></div>
					</div>'
		@canvas.tilepen.append tile_html
		@grab()
		@readmoretxt = @t.find(".fade-overlay-text")
		@t.mouseover =>
			@readmoretxt.html "read more"
		@t.mouseleave =>
			@readmoretxt.html "..."
#		@readmoretxt.mouseover =>
#			@readmoretxt.css {"text-decoration": "underline"}
		@canvas.processSoundcloud @t
		@t.click (e) =>
			if @canvas.overlay.visible is false and ($(e.target).is("a, .sc-img") is false) and @canvas.spannericon.hasClass "fa-chevron-down"
				@canvas.processSoundcloud(@canvas.overlay.overlay) if @canvas.overlay.setData(@)
		@t.find("a, .sc-img").click (e) ->
			e.stopPropagation()

	hide: => @t.hide()

	show: => @t.show()
	
	grab: =>
		@t = $('#'+@tID)

###
	This class is used to handle formatting, displaying, and hiding the
	tile overlay for detailed views.
###
class Overlay
	constructor: (@canvas) ->
		overlay_html = '<div class="overlay-container">
							<div class="loading-overlay">
								<i class="fa fa-fw fa-circle-o-notch fa-spin loading-symbol"></i>
								<div class="loading-text"></div>
							</div>
							 <div class="noresults-overlay">
								<i class="fa fa-fw fa-times-o-notch"></i>
								Sorry, no results match that query.
							</div>
							<div class="overlay">
								<div class="overlay-close">
									<i class="fa fa-times-circle-o"></i>
								</div>
									<div class="overlay-body">
										<div class="overlay-header">
											<div class="overlay-title"></div>
											<div class="overlay-date"></div>
										</div>
										<div class="overlay-content"></div>
									</div>
							</div>
						</div>'
#	Inject overlay HTML into canvas, and then hide it
		@canvas.canvas.append overlay_html
		@overlay = @canvas.canvas.find ".overlay"
		@overlay_title = @canvas.canvas.find ".overlay-title"
		@overlay_header = @canvas.canvas.find ".overlay-header"
		@overlay_content = @canvas.canvas.find ".overlay-content"
		@overlay_date = @canvas.canvas.find ".overlay-date"
		@overlay_close = @canvas.canvas.find ".overlay-close"
		@overlay_loading = @canvas.canvas.find ".loading-overlay"
		@loading_text = @canvas.canvas.find ".loading-text"
		@overlay_noresults = @canvas.canvas.find ".noresults-overlay"
		@overlay_close.click => @hide()
		@hide()

	hide: =>
		@overlay.fadeOut()
		@overlay_content.html("")
		@canvas.tilescroll.css {"opacity": "1"}
		@canvas.canvas.find(".column").css {"overflow-y": ""}
		@visible = false
	
	show: =>
		@pimg = @overlay.imagesLoaded()
		@pimg.always =>
			@overlay.fadeIn()
			h = @overlay_header.outerHeight() + @overlay_content.outerHeight() + 40
			h_max = @canvas.h-60
			if h > h_max
				h = h_max
			@overlay.css {"height": (h+"px")}
			@canvas.tilescroll.css {"opacity": "0.45"}
			@canvas.canvas.find(".column").css {"overflow-y": "hidden"}
			return
#	Note: overlay's visible propetty is set to true inside 
#		@canvas.tilescroll.click callback due to click event bubbling order
	
#	This function will take a tile object as its argument,
#		and then update the information inside the overlay accordingly
	setData: (tile) =>
		if tile.content_l.length > 0
			@overlay_content.html tile.content_l
		else if tile.content.length > 0
			@overlay_content.html tile.content
		else	#	only respond to a tile click if the tile has at least more than "short" content
			@v_trig = false
			return @v_trig
		Prism.highlightAll()
		@overlay.css {"border": ("10px solid "+tile.color)}
		@overlay_close.css {"background": tile.color}
		@overlay_close.hover =>
			@overlay_close.find("i").css {"color": tile.color}
		@overlay_close.mouseleave =>
			@overlay_close.find("i").css {"color": ""}
		@overlay_title.html tile.title
		@overlay_date.html tile.date
		@v_trig = true	#	a meta-variable used to hold a trigger value while a click event bubbles up to the highest-level @canvas div.
		return @v_trig
