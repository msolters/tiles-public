#!/bin/bash

# ( 1 ) first we download and install mongodb
echo "Installing MongoDB:"
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
echo 'deb http://downloads-distro.mongodb.org/repo/ubuntu-upstart dist 10gen' | sudo tee /etc/apt/sources.list.d/mongodb.list
sudo apt-get update
sudo apt-get install mongodb-10gen

# ( 2 ) now we install web server, php support
sudo apt-get install php-pear
sudo pecl install mongo

echo "Make sure to add extension=mongo.so to all php.ini files.  For Apache this is *usually* /etc/php5/apache2/php.ini and/or /etc/php5/cli/php.ini."
