<?php

function update_tile_type_order() {
	require("mongodb.php");
	$cats = $Categories->find()->sort(array("i" => 1));
	foreach($cats as $c) {
		$c_vec[] = $c['type'];
	}
	$t_list = $Tiles->find();
	foreach($t_list as $t) { // gives each tile a type sort index, j
		$t_type = $t['type'];
		$c_j = array_search($t_type, $c_vec);
		echo $t_type. " ".$c_j;
		if ($c_j !== FALSE) {// this is a valid type, let's sort it
			$Tiles -> update($t, array('$set' => array("j" => ($c_j+1))));
		} else { // not a valid type, forget it
			$Tiles -> update($t, array('$unset' => array("type" => 1), '$set' => array("j" => 99999)));//, "j" => 1)));
		}
	}
}

?>
