<?php
/*	this script searches the database using the query and the sort-type criteria
*	passed to it by json from ajax
*	it returns a json info structure representing the results as an array of
*	categories containing arrays of tiles
*/
require("mongodb.php");

function hyperlinksAnchored($text) {
	$text = preg_replace('~(?<!["\'>])(http|ftp)(s)?://((?:[-\w]+\.)+\S+[^,.\s])~', '<a href="$1$2://$3">$3</a>', $text);
	return $text;
//	return preg_replace('~\n\n~', '</br></br>', $text);
}

$tID_list = $_GET['tID_list'];

/*
$query = $_GET['query'];
$sort_type = $_GET['type'];

if (strlen($query) == 0) {
	$t_list = $Tiles->find();
} else {
	$query_array = explode(" ", $query);
	$criteria_array = array("content", "title", "type", "content-s", "content-l");
    $regex_query_array = array();
    foreach($query_array as $q) { // create array of key words
        if  ( ($q != "") && ($q != " ") ) {
            $regex_query_array[] = new MongoRegex("/$q/i");
        }
    }
    $q1 = array();
    foreach($regex_query_array as $rx) {
		$q2 = array();
			 foreach($criteria_array as $crit) {		
			 		$q2[] = array( "$crit" =>  $rx );
			 }
			 $q1[] = array('$or' => $q2);
		}
    $s = array('$and' => $q1);
    $t_list = $Tiles->find($s);
}

//	Generate MongoDB search arrays from the $sort_type value
switch($sort_type) {
	case 'date':
		$sort_array = array("end" => -1, "start" => -1);
		break;
	case 'category':
		$sort_array = array("j" => 1, "i" => 1);
		break;
}

//	Finally, sort results using the above-generated
//	MongoDB search array
$t_list = $t_list->sort($sort_array);


$results = array();	//	Array of categories->tiles
$results_tail = array();	//	The last category.
$category_meta = "";
$current_cat = array();	//	Used to build categories as tiles are sorted through

//	Get the colour of each category from the DB*/
$cats = $Categories->find();
$color_vec = array();
foreach($cats as $c) {
	$col = $c["hsl"];
	$j = $c["i"];
	$color_vec[$j] = $col;
}

$tiles = array();
//	Process results tile-by-tile
foreach($tID_list as $tID) {
	$t = $Tiles->findOne( array( "_id" => new MongoID("$tID") ) );
//	First, get tile parameters
	$tID = $t['_id']->{'$id'};	//tID (str)
	$title = $t['title'];
	$category = $t['type'];			//Category (str)
	$j = $t["j"];				//Category order # (int)
	$color = $color_vec[$j];	//CSS Colour (str)
	$content = hyperlinksAnchored($t['content']);
	$content_l = hyperlinksAnchored($t['content-l']);
	$content_s = hyperlinksAnchored($t['content-s']);
	if ($t['start'] != false) {
		$start_date = date("F Y", $t['start']);
	} else {
		$start_date = "";
	}
	if ($t['end'] != false) {
		$end_date = date("F Y", $t['end']);
	} else {
		$end_date = "";
	}

//	Process datetime or datetime range for output
	if (strlen($t['end']) > 0) {
		if (strlen($t['start']) > 0) {
			$date = date("F Y", $t['start'])."-".date("F Y", $t['end']);
			$fuzzy_date = date("F \of Y", $t['start']);
		} else {
			$date = date("F Y", $t['end']);
			$fuzzy_date = date("F \of Y", $t['end']);
		}
	} else {
		if (strlen($t['start']) > 0) {
			$date = date("F Y", $t['start']);
			$fuzzy_date = date("F \of Y", $t['start']);
		} else {
			$date = "";
			$fuzzy_date = "";
		}
	}
	
//	Construct tile-object for JSON output
	$tile = array(	"tID" => "$tID",
					//"category" => "$category",
					"title" => "$title",
					"content" => "$content",
					"content-s" => "$content_s",
					"content-l" => "$content_l",
					"color" => "$color",
					"date" => "$date",
					"fuzzy-date" => "$fuzzy_date",
					"start-date" => "$start_date",
					"end-date" => "$end_date"
				);
				
	$tiles[] = $tile;
}
/*				
//	Now that we have our data object, we decide where it goes based on
//	the parameters it contains:

//	Category sort logic
	if ( $sort_type == "category" ) {
		if (strlen($category) == 0) {
//	Exclude uncategorized tiles from category search results
			continue;
		} else {
			if ($category != $category_meta) {
//	We've reached a new category!
				if (strlen($category_meta) > 0) {
//	Sub-case: we already have a populated $current_cat; save it!
					$results["$category_meta"] = $current_cat;
					$current_cat = array(); // clear the current cat!
				}
//	All cases: new category
				$category_meta = $category;
			}
//	All cases: save tile
			$current_cat[] = $tile;
		}
	}
	
//	Date sort logic
	if ($sort_type == "date") {
		if ( strlen($fuzzy_date) == 0 ) {
//	No date; put tile in $results_tail
			$results_tail[] = $tile;
		} else {
//	Tile has date; add tile in current category object
			$current_cat[] = $tile;
		}
	}
}

if ($sort_type == "date") {
	$response = array_merge( array("Dated" => $current_cat), array("Undated" => $results_tail) );
}
if ($sort_type == "category") {
	$results["$category_meta"] = $current_cat; // add the last category (might not have been saved if the last tile is categorized, for example)
	$response = $results;
}*/

header('Content-Type: application/json');
echo json_encode(array('tiles' => $tiles));

?>
