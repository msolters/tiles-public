<?php
require("mongodb.php");

$tID = $_GET['tID'];
$dirn = $_GET['dirn'];

$t = $Tiles->findOne(array('_id' => new MongoID($tID)));
$i_t = $t['i'];
$j = $t['j'];

$t_type = $Tiles->find(array("j" => $j))->sort(array("j" => 1, "i" => 1));
$n = 0;
foreach ($t_type as $a) {
	if ($a['i'] == $i_t) {
		break;
	}
	$n++;
}
if ($dirn == 1) {
	$m = $n - 1;
} else {
	$m = $n + 1;
}
$t_l = $t_type->count();
if ( ($m >= 0) && ($t_l > m) ) {
	$n = 0;
	foreach ($t_type as $a) {
		if ($n == $m) {
			$target = $a;
			break;
		}
		$n++;
	}
	$i_m = $target['i'];
	$Tiles -> update($target, array('$set' => array("i" => $i_t)));
	$Tiles -> update($t, array('$set' => array("i" => $i_m)));
}

?>
