<?php
require("mongodb.php");

$c_vec = $Categories->find()->sort(array("i" => 1));
$output = "";
foreach($c_vec as $c) {
	$label = $c['type'];
	$output .= "<div class='row cat-row'>
					<i onclick='javascript: delete_category(this);' class='fa fa-trash-o red-btn cat-arrow'></i><i onclick='javascript: move_category(this, 1);' class='fa fa-arrow-up grey-btn cat-arrow'></i><i onclick='javascript: move_category(this, 0);' class='fa fa-arrow-down grey-btn cat-arrow'></i><input type='text' class='tile-e-input borderless cat-input' onkeyup='javascript: show_save_btn(\"categories\");' cur_type='$label' placeholder='New Category...' value='$label'>
				</div>";
}
echo $output;

?>
