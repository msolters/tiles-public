<?php

require("mongodb.php");
require("update-tile-type-order.php");

$tID = $_POST['tID'];
$start = $_POST['start'];
$end = $_POST['end'];
$mongo_start = strtotime("1 ".$start." 00:00:00");
$mongo_end = strtotime("1 ".$end." 00:00:00");

$title = $_POST['title'];
$title = htmlentities($title, ENT_QUOTES);
$type = htmlentities($_POST['type'], ENT_QUOTES);

$content = $_POST['content'];
$content_s = $_POST['content_s'];
$content_l = $_POST['content_l'];

$Tiles -> update(array('_id' => new MongoID($tID) ), array('$set' => array("start" => $mongo_start, "end" => $mongo_end, "type" => "$type", "title" => "$title", "content" => "$content", "content-s" => "$content_s", "content-l" => "$content_l")));

update_tile_type_order();

?>
