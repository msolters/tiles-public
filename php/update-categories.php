<?php

require("mongodb.php");
require("update-tile-type-order.php");

//Get all vars that are passed from the JS
$dname = $_GET['dname'];
$cats = $_GET['c_vec'];
$old_cats = $_GET['old_c_vec'];
$i = 1;

//update User information
$current_ = $User -> findOne();
// "dname" => array('$nin' => array(' ', '', null))
$current_dname = $current_["dname"];
$User -> drop();
$User -> save(array("dname" => "$dname"));

// update Category information
$hue_f = 360/count($cats);
$hue = 0;

foreach($cats as $c) {
	$color = "hsl($hue, 50%, 50%)";
	$hue += $hue_f;
	$k = $i - 1; 
	$old_type = $old_cats[$k];
	$new_type = htmlentities("$c");
	
	if (strlen($old_type) > 0) {
		$Categories->update(array("type" => "$old_type"), array('$set' => array("type" => "$new_type", "i" => $i, "hsl" => "$color") ));
		$Tiles->update(array("type" => "$old_type"), array('$set' => array("type" => "$new_type")), array("multiple"=>true));
	} else {
		$Categories->insert( array("type" => "$new_type", "i" => $i, "hsl" => "$color" ) );
	}
	$i++;
}

$db_cats = $Categories->find();
foreach($db_cats as $dbc) {
	$dbc_type = $dbc['type'];
	if (!in_array($dbc_type, $cats)) {
		$Categories->remove(array("type" => "$dbc_type"));
	}
}

update_tile_type_order();

?>
