<?php
require("mongodb.php");
$tID = new MongoID($_GET['tID']);

if ($tID) {
	$t = $Tiles->findOne(array('_id' => $tID));
	$i_t = $t['i'];
	$Tiles -> remove(array("_id" => $tID));
	$Tiles -> update(array("i" => array('$gt' => $i_t)), array('$inc' => array("i" => -1)), array("multiple"=>1));
}
?>
