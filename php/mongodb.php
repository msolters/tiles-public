<?php
include("../user.php");
include("user.php"); // in the case that this file is called from within index.php itself

// MongoDB server and DB name (do not edit below this line)
$dbhost = 'localhost';  
$dbname = 'tiles';
// Connect to database  
$m = new Mongo("$dbhost"); 
$db = $m->$dbname;

$tile_collection = $username."Tiles";
$Tiles = $db->$tile_collection;

$category_collection = $username."Categories";
$Categories = $db->$category_collection;

$user_collection = $username."User";
$User = $db->$user_collection;
$current_ = $User -> findOne();
$dname = $current_["dname"];

?>
