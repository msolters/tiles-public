<?php

require("mongodb.php");
require("update-tile-type-order.php");

$start = $_GET['start'];
$mongo_start = strtotime("1 ".$start." 00:00:00");
$end = $_GET['end'];
$mongo_end = strtotime("1 ".$end." 00:00:00");

$title = $_GET['title'];
$title = htmlentities($title, ENT_QUOTES);
$type = $_GET['type'];
$type = htmlentities($type, ENT_QUOTES);

$content = $_GET['content'];
$content_s = $_GET['content_s'];
$content_l = $_GET['content_l'];

$N = $Tiles->find()->count();
$N += 1;
$Tiles -> insert( array("start" => $mongo_start, "end" => $mongo_end, "title" => "$title", "type" => "$type", "content" => "$content", "content-s" => "$content_s", "content-l" => "$content_l", "i" => $N) );

update_tile_type_order();

?>
