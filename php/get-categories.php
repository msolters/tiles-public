<?php
require("mongodb.php");

$c_vec = $Categories->find()->sort(array("i" => 1));
$output = array();
foreach($c_vec as $c) {
	$label = $c['type'];
	$color = $c['hsl'];
	//$output .= '<div class="category-choice" style="color: '.$color.'; border-color: '.$color.'">'.$label.'</div>';
	$output[$label] = $color;
}

header('Content-Type: application/json');
echo json_encode(array('category_colours' => $output));
?>
