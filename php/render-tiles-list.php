<?php

require("mongodb.php");
$t_list = $Tiles -> find() -> sort(array("j" => 1, "i" => 1));//sort(array("start" => -1, "end" => -1));
$output = "";
$months = array("Month", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
	

foreach($t_list as $t) {
	$tID = $t['_id']->{'$id'};
	$date_start_style = "style='display: none;'";
	$m_s =$m_e = $y_s = $y_e = "";
	if (strlen($t['start']) > 0) {
		$m_s = date("F", $t['start']);
		$y_s = date("Y", $t['start']);
		$date_start_style = "";
	}
	if (strlen($t['end']) > 0) {
		$m_e = date("F", $t['end']);
		$y_e = date("Y", $t['end']);
		$date_start_style = "";
	}
	$output .= "<div class='tile-e' id='".$tID."'>
					<div class='row tile-e-header'>
						<input class='tile-e-title tile-e-input borderless' type='text' placeholder='Title' value='".$t['title']."' onkeyup='javascript: show_save_btn(\"".$tID."\");'>
					</div>
					<div class='row tile-e-dates'>
						<span class='date-start animate' ".$date_start_style.">
							<select class='borderless tile-e-input month-start' onchange='javascript: show_save_btn(\"".$tID."\");'>";
								foreach($months as $m) {
									if ($m == $m_s) {
										$output .= "<option selected>";
									} else {
										$output .= "<option>";
									}
									$output .= $m."</option>";
								}
		$output .= "		</select>
							<input type='num' class='borderless tile-e-input year-start' placeholder='Year' onkeyup='javascript: show_save_btn(\"new\");' value='".$y_s."'>
						</span>
						<select class='borderless month-end tile-e-input' onchange='javascript: check_dates(\"".$tID."\");'>";
							foreach($months as $m) {
								if ($m == $m_e) {
									$output .= "<option selected>";
								} else {
									$output .= "<option>";
								}
								$output .= $m."</option>";
							}
	$output .="		</select>
						<input type='num' class='borderless tile-e-input year-end' onkeyup='javascript: check_dates(\"".$tID."\");' placeholder='Year' value='".$y_e."'>

					</div>
					<div class='row tile-e-type'>
						<select class='select-input tile-e-input borderless' cur_type='".$t['type']."' onchange='javascript: show_save_btn(\"".$tID."\");'></select>
					</div>
					<div class='row tile-e-body'>

						<div class='tile-e-content'>
							<div class='row content-heading' onclick='javascript: $(\"#".$tID." .tile-e-content-text\").slideToggle();'>
								Common <i class='fa fa-fw fa-cog grey-btn' ></i>
							</div>
							<textarea class='tile-e-content-text tile-e-input borderless' placeholder='Put your HTML here!' rows=5 onkeyup='javascript: show_save_btn(\"".$tID."\");' >".$t['content']."</textarea>
						</div>
						
						 <div class='tile-e-content-s'>
							<div class='row content-heading' onclick='javascript: $(\"#".$tID." .tile-e-content-s-text\").slideToggle();'>
								Short <i class='fa fa-fw fa-cog grey-btn'></i>
							</div>
							<textarea class='tile-e-content-s-text tile-e-input borderless' placeholder='Put your HTML here!' rows=5 onkeyup='javascript: show_save_btn(\"".$tID."\");' >".$t['content-s']."</textarea>
						</div>
						
						 <div class='tile-e-content-l'>
							<div class='row content-heading' onclick='javascript: $(\"#".$tID." .tile-e-content-l-text\").slideToggle();'>
								Long <i class='fa fa-fw fa-cog grey-btn'></i>
							</div>
							<textarea class='tile-e-content-l-text tile-e-input borderless' placeholder='Put your HTML here!' rows=8 onkeyup='javascript: show_save_btn(\"".$tID."\");' >".$t['content-l']."</textarea>
						</div>
						
					</div>
					<div class='tile-e-save'>
						<i onclick='javascript: update_tile(\"".$tID."\");' class='fa fa-save fa-2x green-btn tile-e-btn-2 '></i>
					</div>
					<div class='tile-e-del '>
						<i onclick='javascript: delete_tile(\"".$tID."\");' class='fa fa-trash-o fa-2x red-btn tile-e-btn-1 '></i>
					</div>
					<div class='tile-e-up'>
						<i onclick='javascript: move_tile(\"".$tID."\", 1);' class='fa fa-arrow-up fa-2x grey-btn tile-e-btn-4 '></i>
					</div>
					<div class='tile-e-down'>
						<i onclick='javascript: move_tile(\"".$tID."\", 0);' class='fa fa-arrow-down fa-2x grey-btn tile-e-btn-3 '></i>
					</div>
				</div>
";
}
echo $output;
?>
