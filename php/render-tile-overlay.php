<?php

require("mongodb.php");
$tID = $_GET['tID'];

if (!isset($tID)) {
	exit();
}

$t = $Tiles->findOne(array("_id" => new MongoID($tID) ) );

if ($t) {
	if (strlen($t['end']) > 0) {
		if (strlen($t['start']) > 0) {
			$date = "(".date("m/Y", $t['start'])."-".date("m/Y", $t['end']).")";
			//$fuzzy_date = date("F \of Y", $t['start']);
		} else {
			$date = "(".date("F, Y", $t['end']).")";
			//$fuzzy_date = date("F \of Y", $t['end']);
		}
	} else {
		if (strlen($t['start']) > 0) {
			$date = "(".date("F, Y", $t['start']).")";
			//$fuzzy_date = date("F \of Y", $t['start']);
		} else {
			$date = "";
			//$fuzzy_date = "";
		}
	}
	if (strlen($t['content-l']) > 0) {
		$overlay_content = $t['content-l'];
	} else {
		$overlay_content = $t['content'];
	}
	$output = "<div id='overlay-panel' class='overlay-panel'>
					<div class='overlay-close-btn' onclick='javascript: hide_tile_overlay();'>
						<i class='fa fa-chevron-up fa-2x grey-btn'></i>
					</div>

					<div class='overlay-title'>
						".$t['title']." <span class='overlay-timestamp'>".$date."</span>
					</div>

					<div class='overlay-content'>
						".$overlay_content."
					</div>
				</div>";
	echo $output;
} else {
	exit();
}

?>
