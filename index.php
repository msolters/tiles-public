<!DOCTYPE html>
<html>
	<head>
	    <meta name="author" content="Mark Solters">
	    <meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <!--<link href="img/favicon.ico" rel="icon" >-->
	
	    <title>User Name</title>

		<link href='http://fonts.googleapis.com/css?family=Roboto:400,700,400italic|Roboto+Slab:400,700' rel='stylesheet' type='text/css'>
		<link href="css/font-awesome.css" rel="stylesheet"/>
		<link href="css/prism.css" rel="stylesheet" type="text/css" />
		<link href="css/tiles.css" rel="stylesheet" type="text/css" />
		<!-- Search Icon made by Adam Whitcroft from www.flaticon.com -->
		<!-- Delete Icon made by Adam Whitcroft from www.flaticon.com -->

	</head>

	<body>

	<div id="example-canvas"></div>

	<!-- where the magic happens -->
	<script src="js/jquery.js"></script>
	<script src="js/jquery.mousewheel.min.js"></script>
	<script src="js/imagesloaded.js"></script>
	<script src="js/prism.js"></script>
	<script src="js/tiles.js"></script>
	
	<script type="text/javascript" language="javascript">
		var main_tilepen = new Tilepen( $("#example-canvas"), true );
	</script>

	</body>
</html>
