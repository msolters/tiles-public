<?php
include("../php/mongodb.php");
?>

<!DOCTYPE html>
<html>
	<head>
	    <meta name="author" content="Mark Solters">
	    <meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=5">
        <!--<link href="img/favicon.ico" rel="icon" >-->
	
	    <title>Tiles - Manage My Tiles</title>

		<link href="../css/font-awesome.css" rel="stylesheet"/>
		<link href="../css/tiles-backend.css" rel="stylesheet" type="text/css" />
	</head>

	<body>
		<div id="layout">
			<div id="my-cats-header">
				Control Panel
			</div>

			<div id="tiles-categories" class="mc">
				<div class='tile-e' id='categories'>
					<div id="name-title" class='row tile-e-header title'>
						Display Name
					</div>
					<div id="name-change" class="row">
						<input type="text" id="display-name" onkeyup="javascript: show_save_btn('categories');" class="tile-e-input borderless" placeholder="Display Name" value="<?php echo $dname; ?>"/>
					</div>

					<div id="pw-title" class='row tile-e-header title' onclick="javascript: $('#login-change').slideToggle();">
						Backend Login <i class="fa fa-fw fa-refresh grey-btn" ></i>
					</div>
					<div id="login-change">
					<div id="user-name-change" class="row">
						<input type="text" id="user-name" onkeyup="javascript: show_save_btn('categories');" class="tile-e-input borderless" placeholder="New Username"/>
					</div>
					<div id="pw-change" class="row">
						<input type="password" id="password" onkeyup="javascript: show_save_btn('categories');" class="tile-e-input borderless" placeholder="New Password"/>
					</div>
					</div>

					<div id="category-title" class='row tile-e-header title'>
						Categories
					</div>
					<div id='cat-holder'></div>
					<div class='tile-e-save'>
						<i onclick='javascript: update_categories();' class='fa fa-save fa-2x green-btn tile-e-btn-2 '></i>
					</div>
					<div class='tile-e-add'>
						<i onclick='javascript: add_category();' class='fa fa-plus fa-2x green-btn tile-e-btn-1 '></i>
					</div>
				</div>
			</div>

			 <div id="my-tiles-header">
				My Tiles
			</div>
			<div id="tiles-new">
				<i id='new-tile-btn' class="green-btn fa fa-2x fa-plus" onclick="javascript: show_new_tile();"></i>
				<div id="new-tile-holder" class="mc">
					<div class='tile-e' id='new'>
						<div class='row tile-e-header'>
							<input class='tile-e-title tile-e-input borderless' type='text' placeholder='Title' onkeyup='javascript: show_save_btn(&quot;new&quot;);'>
						</div>
						<div class='tile-e-dates'>
							<span class="date-start animate" style="display: none;">
								<select class='borderless tile-e-input month-start' onchange='javascript: show_save_btn(\"new\");'>
									<option>Month</option>
									<option>January</option>
									<option>February</option>
									<option>March</option>
									<option>April</option>
									<option>May</option>
									<option>June</option>
									<option>July</option>
									<option>August</option>
									<option>September</option>
									<option>October</option>
									<option>November</option>
									<option>December</option>
								</select>
								<input type='number' class='borderless tile-e-input year-start' placeholder="Year" onkeyup='javascript: show_save_btn(\"new\");'>
							</span>
							<select class='borderless tile-e-input month-end' onchange="javascript: check_dates('new');">
								<option>Month</option>
								<option>January</option>
								<option>February</option>
								<option>March</option>
								<option>April</option>
								<option>May</option>
								<option>June</option>
								<option>July</option>
								<option>August</option>
								<option>September</option>
								<option>October</option>
								<option>November</option>
								<option>December</option>
							</select>
							<input type='number' class='borderless tile-e-input year-end' placeholder="Year" onkeyup="javascript: check_dates('new');">

						</div>
						<div class='row tile-e-type'>
							<!--<input class='tile-e-sort-type tile-e-input borderless' type='text' placeholder='Tile Type' onkeyup='javascript: show_save_btn(&quot;new&quot;);'>-->
							<select class='select-input tile-e-input borderless'></select>
						</div>
						<div class='row tile-e-body'>

							<div class='tile-e-content'>
								<div class='row content-heading' onclick='javascript: $("#new .tile-e-content-text").slideToggle();'>
									Common <i class='fa fa-fw fa-cog grey-btn'></i>
								</div>
								<textarea class='tile-e-content-text tile-e-input borderless' placeholder='Put your HTML here!' onkeyup='javascript: show_save_btn(&quot;new&quot;);' rows=5></textarea>
							</div>
						
							 <div class='tile-e-content-s'>
								<div class='row content-heading' onclick='javascript: $(&quot;#new .tile-e-content-s-text&quot;).slideToggle();'>
									Short <i class='fa fa-fw fa-cog grey-btn '></i>
								</div>
								<textarea class='tile-e-content-s-text tile-e-input borderless' placeholder='Put your HTML here!' rows=5 style="display: none" onkeyup='javascript: show_save_btn(&quot;new&quot;);' ></textarea>
							</div>
						
						 <div class='tile-e-content-l'>
							<div class='row content-heading' onclick='javascript: $(&quot;#new .tile-e-content-l-text&quot;).slideToggle();'>
								Long <i class='fa fa-fw fa-cog grey-btn'></i>
							</div>
							<textarea class='tile-e-content-l-text tile-e-input borderless' placeholder='Put your HTML here!' rows=8 style="display: none" onkeyup='javascript: show_save_btn(&quot;new&quot;);' ></textarea>
						</div>
						</div>
					 	<div class='tile-e-save'>
							<i onclick='javascript: create_tile();' class='fa fa-save fa-2x green-btn tile-e-btn-2 '></i>
						</div>
						<div class='tile-e-cancel'>
							<i onclick='javascript: hide_new_tile();' class='fa fa-trash-o fa-2x red-btn tile-e-btn-1 '></i>
						</div>
					</div>
				</div>
			</div>

			<div id="tiles-holder" class="mc">
			</div>
		</div>
		<script src="../js/jquery.js"></script>
		<script src="js/tiles-backend.js"></script>
	</body>
</html>
