function render_tiles_list() {
	$.ajax({
		type: "GET",
		url: "../php/render-tiles-list.php",
		success: function(data) {
			$("#tiles-holder").html(data);
			render_category_selectors();
			$("#tiles-holder .tile-e").each(function() {
				 render_content_inputs( $(this).attr("id") );
			});
		}
	});
}

function render_content_inputs(tID) {
	var c = $("#"+tID+" .tile-e-content-text").val();
	 var c_s = $("#"+tID+" .tile-e-content-s-text").val();
	 var c_l = $("#"+tID+" .tile-e-content-l-text").val();
	 if ((c_l.length > 0) && (c_s.length > 0)) {
	 	 $("#"+tID+" .tile-e-content-text").hide();
	 }
	 if (c_s.length == 0) {
	 	 $("#"+tID+" .tile-e-content-s-text").hide();
	 }
	 if (c_l.length == 0) {
	 	 $("#"+tID+" .tile-e-content-l-text").hide();
	 }
}

function update_tile(tID) {
	var content = $('#'+tID+' .tile-e-content-text').val();
	var content_s = $('#'+tID+' .tile-e-content-s-text').val();
	var content_l = $('#'+tID+' .tile-e-content-l-text').val();
	var dates = create_timestamp(tID);
	var timestamp_1 = dates[0];
	var timestamp_2 = dates[1];
	var title = $('#'+tID+' .tile-e-title').val();
	var type = $('#'+tID+' .select-input').val();
	$.ajax({
		type: "POST",
		url: "../php/update-tile.php",
		data: {tID: tID, start: timestamp_1, end: timestamp_2, title: title, type: type, content: content, content_s: content_s, content_l: content_l},
		success: function(data) {
			hide_save_btn(tID);
		}
	});
}

$(document).ready(function() {
	render_tiles_list();
	render_categories();
});

function delete_tile(tID) {
	// this needs some kind of confirmation
	$.ajax({
		type: "GET",
		url: "../php/delete-tile.php",
		data: {tID: tID},
		success: function(data) {
			render_tiles_list();
		}
	});
}

function create_timestamp(tile) {
	var y_s = $('#'+tile+' .year-start').val();
	var m_s = $('#'+tile+' .month-start').val();
	if (m_s == "Month") {
		m_s = "";
	}
	var timestamp_start = m_s;
	if (y_s.length > 0) {
		timestamp_start += " "+y_s;
	}
	var y_e = $('#'+tile+' .year-end').val();
	var m_e = $('#'+tile+' .month-end').val();
	if (m_e == "Month") {
		m_e = "";
	}
	var timestamp_end = m_e;
	if (y_e.length > 0) {
		timestamp_end += " "+y_e;
	}
	var output = new Array();
	output.push(timestamp_start);
	output.push(timestamp_end);
	return output;
}

function create_tile() {
	var content = $('#new .tile-e-content-text').val();
	var content_s = $('#new .tile-e-content-s-text').val();
	var content_l = $('#new .tile-e-content-l-text').val();

	var dates = create_timestamp("new");
	var timestamp_1 = dates[0];
	var timestamp_2 = dates[1];

	var title = $('#new .tile-e-title').val();
	var type = $('#new .select-input').val();
	if ( (content.length == 0) && (title.length == 0) && (type.length == 0) && (timestamp_1.length == 0) && (timestamp_2.length == 0)) {
		return;
	}
	$.ajax({
		type: "GET",
		url: "../php/create-tile.php",
		data: {start: timestamp_1, end: timestamp_2, type: type, title: title, content: content, content_s: content_s, content_l: content_l},
		success: function(data) {
			render_tiles_list();
			hide_new_tile();
		}
	});
}

function move_tile(tID, dirn) {
	$.ajax({
		type: "GET",
		url: "../php/move-tile.php",
		data: {tID: tID, dirn: dirn},
		success: function(data) {
			var t = $('#'+tID);
			if (dirn == 1) {
				t.prev().before(t);
			} else {
				t.next().after(t);
			}
		}
	});
}

function show_new_tile() {
	$('#new-tile-btn').hide();
	$('#new-tile-holder').slideDown();
	$('#new .tile-e-content-text').slideDown();
}

function hide_new_tile() {
	$('#new-tile-btn').show();
	$('#new-tile-holder').slideUp();
	//$('#new .tile-e-content-text, #new .tile-e-date, #new .tile-e-title').val("");
	$('#new .tile-e-content-text, #new .tile-e-content-s-text, #new .tile-e-content-l-text, #new .year-start, #new .year-end, #new .tile-e-sort-type, #new .tile-e-title').val("");
	check_dates("new");
	$(' #new .month-start, #new .month-end ').val("Month");
}

function show_save_btn(tID) {
	$('#'+tID+' .tile-e-save').show();
}

function hide_save_btn(tID) {
	$('#'+tID+' .tile-e-save').hide();
}

function render_categories() {
	$.ajax({
		type: "GET",
		url: "../php/render-tile-categories.php",
		success: function(data) {
			$('#cat-holder').html(data);
			hide_save_btn("categories");
		}
	});
}

function update_categories() {
	var display_name = $("#display-name").val();
	var cats = new Array();
	var old_cats = new Array();
	var uname = $('#user-name').val();
	var pw = $('#password').val();
	$(".cat-input").each(function() {
		var val = $(this).val();
		if (val.length > 0) {
			if (cats.indexOf(val) > -1) {
				alert("uh oh! you have a duplicate type.  cut that shit out.");
				return;
			} else {
				cats.push($(this).val());
				old_cats.push($(this).attr('cur_type'));
			}
		}
	});
	$.ajax({
		type: "GET",
		url: "../php/update-categories.php",
		data: {c_vec: cats, old_c_vec: old_cats, dname: display_name},
		success: function(data) {
			render_categories();
			render_category_selectors();
			render_tiles_list();
		}
	});
	if ( (uname.length > 0) || (pw.length > 0) ) {
		$.ajax({
			type: "GET",
			url: "../php/update-login.php",
			data: {uname: uname, pw: pw},
			success: function(data) {
				location.reload();
			}
		});
	}
}

function add_category() {
	$('#cat-holder').append("<div class='row'><i onclick='javascript: move_category(this, 1);' class='fa fa-arrow-up grey-btn cat-arrow'></i><i onclick='javascript: move_category(this, 0);' class='fa fa-arrow-down grey-btn cat-arrow'></i><input type='text' class='tile-e-input borderless cat-input' placeholder='New Category...'></div>");
	show_save_btn("categories");
}

function move_category(arrow_btn, dirn) {
	var r = $(arrow_btn).parent();
	if (dirn == 1) {
		r.prev().before(r);
	} else {
		r.next().after(r);
	}
	show_save_btn("categories");
}

function delete_category(t) {
	$(t).parent().remove();
	update_categories();
}

function render_category_selectors() {
	$.ajax({
		type: "GET",
		url: "../php/render-category-dropdown.php",
		success: function(data) {
			$(".select-input").each(function() {
				$(this).html(data); // update the options
				var init = $(this).attr('cur_type'); // select the right one
				if (init) {
					$(this).children().filter(function() { // this blurb selects the proper contact type by searching for a matching string
						return $(this).text() == init; 
					}).prop('selected', true);
				}
			});
		}
	});
}

function check_dates(t) {
	show_save_btn(t);
	var m_e = $('#'+t+' .month-end').val();
	if (m_e == "Month") {
		m_e = "";
	}
	var y_e = $('#'+t+' .year-end').val();
	if ( (m_e.length > 0) && (y_e.length > 0) ) {
		$('#'+t+' .date-start').show("fast");
	} else {
		$('#'+t+' .date-start').hide();
		$('#'+t+' .month-start').val("Month");
		$('#'+t+' .year-start').val("");
	}
}
