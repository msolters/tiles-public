jQuery.fn.outerHTML = function() { // this merely returns the exact and entire html of any given jquery object
	return (this[0]) ? this[0].outerHTML : '';  
};

var v_scroll = false;
function render_tile_display_new() {
	if ($('#tile-pen').is(':empty')) {
		return;
	}
	var tile_stack_height = 0;	// first we compute various parameters about column size
	var tile_tail_stack_height = 0;
	$("#tile-pen").append("<div class='column'></div>");
	$('#tile-pen').show();
	var column_width = $("#tile-pen .column").outerWidth();
	var max_column_height = $("#tile-pen .column").height();
		
	$("#tile-pen .column").remove();
	if ( $(window).width() < 700 ) {
		if (v_scroll == false) { 
			v_scroll = true;
			$("#tile-display .column, #tile-display .column-header, #tile-display .tile").remove();
		} else { // the window has already been rendered for vertical scroll mode
			$('#tile-pen').hide();
			return;
		}
	} else {
		if (v_scroll == true) {
			if ( (document.activeElement.tagName.toLowerCase() == "iframe") && (window.innerHeight == screen.height) ) {
				return;
			}
			$("#tile-display .column, #tile-display .column-header, #tile-display .tile").remove();
			v_scroll = false;
		}
	}
	var c = 1; // column number
	var	c_vec = new Array();
	var tid_vec = new Array();

	var c_tail = 1;
	var c_vec_tail = new Array();
	var tid_vec_tail = new Array; // this is used to store the id of Undated tiles in date sort

	var tile_type_meta = "";

	$('#tile-pen .tile').each(function() {
		var tID = $(this).attr("tid");	// first we compute various parameters from our extant tile in #tile-pen
		var tile_type = $("#pen-"+tID+" .type-column-header").html();
		var tile_fuzzy_date = $("#pen-"+tID+" .fuzzy-column-header").html();
		var tile_height = parseFloat( $(this).outerHeight() ) + parseFloat( $(this).css("margin-bottom") ) - 5;

		var tail_tile = false;
		if (v_scroll == true) { // we are in vertical scroll mode
			iframe_clear();
			if (sort_type == "date") {
				if (tile_fuzzy_date == "Undated") { // this tile is undated, put it in the tail
					tid_vec_tail.push(tID);
					c_vec_tail.push(c_tail);
					tail_tile = true;
				}
			}
			if (sort_type == "type") {
				if ( (tile_type !== tile_type_meta) && (tile_type_meta.length > 0) ) { // this tile is a new category let's create a new tile
					c++;
				}
				tile_type_meta = tile_type;
			}
		} else { // we are in horizontal scroll mode
			if (sort_type == "date") {
				if (tile_fuzzy_date == "Undated") { // this tile is undated, put it in the tail
					tile_tail_stack_height += tile_height;
					if (tile_tail_stack_height >= max_column_height) {
						c_tail++;
						tile_tail_stack_height = tile_height;
					}
					tid_vec_tail.push(tID);					
					c_vec_tail.push(c_tail);
					tail_tile = true;
				} else {
					tile_stack_height += tile_height;
				}
			}
			if (sort_type == "type") {
				if ( (tile_type !== tile_type_meta) && (tile_type_meta.length > 0) ) { // this tile is a new category let's create a new tile
					tile_type_meta = tile_type;
					c++;
					tile_stack_height = tile_height;
				} else {
					tile_stack_height += tile_height;
					if ( tile_type_meta.length == 0 ) {
						tile_type_meta = tile_type;
					}
				}
			}
			if (tile_stack_height >= max_column_height) {// the tiles go to a new column because we're out of space
				c++;
				tile_stack_height = tile_height;
			}	
		}
		if (tail_tile == false) { 		// only store the tile if it wasn't a tail column
			tid_vec.push(tID);
			c_vec.push(c);
		}
	});
	if ( (tid_vec_tail.length > 0) && (sort_type == "date") ) {		// add tail to the tid vector, and create corresponding number of c_vec elements
		tid_vec = tid_vec.concat(tid_vec_tail);
		var tail_c = c_vec[c_vec.length - 1];
		for (var n = 0; n < tid_vec_tail.length; n++) {
			c_vec_tail[n] = c_vec_tail[n] + tail_c;
		}
		c_vec = c_vec.concat(c_vec_tail);
	}
	$('#tile-pen').hide();
	
	$('#tile-display .column, #tile-display .column-header').hide();
	$('#tile-display .column').css({"border-right": "2px solid #aaa", "max-width": ""});
	for (var i = 0; i < tid_vec.length; i++) {		// loop over all possible tiles in the #tile-pen
		var c = c_vec[i];
		var tID = tid_vec[i];
		var left_spacing = (c-1) * column_width;
		/*if ($(window).width() > 450) {
			left_spacing += 9;
		}*/

		if ( $('#column-'+c).length == 0 ) { // column does not exist, create it!
			$('#tile-display').append("<div id='"+c+"-column-header' class='column-header' style='left: "+(left_spacing+10)+"px;'></div><div id='column-"+c+"' class='column' style='left: "+left_spacing+"px'></div>");
		} else {
			$('#column-'+c).css("left", ((left_spacing)+"px"));
		}
		$('#column-'+c).show(); 
		if ( $('#'+tID).length == 0 ) { // tiles does not exist, append it
			var tile_type = $("#pen-"+tID+" .type-column-header").html();  // get details from the pen
			var tile_fuzzy_date = $("#pen-"+tID+" .fuzzy-column-header").html();
			var tile_style = $('#pen-'+tID).attr('style');	// we won't use need all of them
			var tile_content = $('#pen-'+tID).html();
			$('#column-'+c).append("<div class='tile animate' id='"+tID+"' style='"+tile_style+"' onclick='javascript: render_tile_overlay(\""+tID+"\");'>"+tile_content+"</div>");
		} else {							// tile does exist!
			var cID = $('#'+tID).parent().attr("id");
			$('#column-'+c).append($('#'+tID));
			$('#'+tID).show();
		}
	}

	label_meta = ""; // reset dummy meta var
	for (var i = 1; i <= c; i++) { // generates column headers
		var header = "";
		if ( (sort_type == "all") || (sort_type == "date") ) {
			var c_t = $("#column-"+i+" .fuzzy-column-header");
		} else {
			var c_t = $("#column-"+i+" .type-column-header");
		}
		c_t.each(function() {
			if ($(this).html().length > 0) {
				header = $(this).html();
				return false;
			}
		});
		if ( ( (header.length > 0) && (header == label_meta) ) && (sort_type == "type") ) {
			//alert($('#column-'+(i-1)).css( "border-right-style" ) );
			if ( $('#column-'+(i-1)).css( "border-right-style" ) !== "none") { // if a column has a border, remove it, and increase its width correspondingly
				$('#column-'+(i-1)).css( {"border-right": "none", "max-width": ((parseFloat( $('#column-'+(i-1)).css("max-width")) + 2) + "px")} );
			}
		}
		if ( (header.length > 0) && (header !== label_meta) ) {
			$("#"+i+"-column-header").html(header);
			$("#"+i+"-column-header").show();
			label_meta = header;
		}
	}

	if (v_scroll == false) {
		iframe_set_track();
	} else {
		/*var y_0 = 50;
		if ($(window).width < 450) {
			y_0 = 65;
		}*/
		$('#tile-display .column').scroll(function() {
			var dy = $(this).scrollTop();
			var c = $(this).attr("id");
			c = c.split("-")[1];
			//$("#"+c+"-column-header").css({"top": ((y_0 - dy) +"px")});
			if (dy > 7) {
				$("#"+c+"-column-header").hide();
			} else {
				$("#"+c+"-column-header").show();
			}
			$(this).find(".ibody").each(function() {
				var bID = $(this).attr("id");
				var bID_parts = bID.split("-");
				var m = bID_parts[1];
				var p = GetScreenCordinates(this);
				var y = p.y - dy;
				$('#ihead-'+m).css({"top": (y+"px")});
			});
		});
	}

	$("#load-overlay").hide();
}

function iframe_clear() {
	$(".ihead, .ibody").remove();	// unwatch?
}

function GetScreenCordinates(obj) { // by Mudassar Ahmed Khan
	var p = {};			// http://www.aspsnippets.com/Articles/Get-Absolute-Position-Screen-Cordinates-of-HTML-Elements-using-JavaScript.aspx
	p.x = obj.offsetLeft;		// pretty self-explanatory
	p.y = obj.offsetTop;
	while (obj.offsetParent) {
		p.x = p.x + obj.offsetParent.offsetLeft;
		p.y = p.y + obj.offsetParent.offsetTop;
		if (obj == document.getElementsByTagName("body")[0]) {
			break;
		} else {
			obj = obj.offsetParent;
		}
	}
	return p;
}

function iframe_set_track() {
	var i = 0; 
	
	$("#tile-display iframe").each(function() {	// if there's any iframes in the display area, we move them to absolutely positioned sprites in the layout
		i++; // ratchet it up one!
		var iframe = $(this).get(0);
		var max_w = $(this).attr("iwidth");//width(); // set the body placeholder to just be locked at the original media size
		var max_h = $(this).attr("iheight");	// maybe eventualy user parameter listener on iframe in pen to monitor resize?
		$(this).width(max_w);
		$(this).height(max_h);
		var x = $(this).offset().left;
		var y = $(this).offset().top;
		$(this).after("<div id='ibody-"+i+"' class='ibody' style='position: relative;'><div id='ihead-"+i+"' class='ihead'></div></div>");
		$("#ihead-"+i).append($(this));
		$("#layout").append( $("#ihead-"+i) );
		$("#ihead-"+i).css({"left": x, "top": y});
		$("#ibody-"+i).css({"width": max_w, "height": max_h, "max-width": max_w, "max-height": max_h});
	});

	$("#layout .ihead").each(function() {
		var hID = $(this).attr("id");
		var id_boom = hID.split("-");
		var m = id_boom[1];
		var bID = "ibody-"+m;
		var b = document.getElementById(bID);
		var p = GetScreenCordinates(b);
		$(this).css({"left": p.x, "top": p.y, "width": $("#"+bID).width(), "height": $("#"+bID).height(), "max-width": $("#"+bID).width(), "max-height": $("#"+bID).height()});
	});
}



/*function render_tile_display() { // takes tiles in #tile-pen and presents them according to user selected sort method
	$("#tile-display").append("<div class='column'></div>");
	$('#tile-display').show();
	var t_w = $("#tile-display .column").outerWidth() - 2;
	var column_h = $("#tile-display .column").height();
	$('#tile-display').hide();	
	$("#tile-display .column").remove();
	var c = 0;
	var left_spacing = parseFloat( $('#tile-display').css("padding-left") );
	var H = parseFloat( $("#layout").height() - 60 );
	var h = H+10; // h_new must be bigger than H
	var h_meta = true;
	var h_vec = new Array();
	var output = "";//<div id='column-"+c+"' class='column'>"; // column 1
	$("#tile-pen").show();
	var label_meta = "";
	var tail = false; // this becomes true when we reach our first undated tile
	var tail_meta = true;	

	$('#tile-pen .tile').each(function() {
		var tID = $(this).attr("tid");
		var t_style = $(this).attr('style');
		var d_h = parseFloat( $(this).outerHeight() ) + parseFloat( $(this).css("margin-bottom") );
		var h_new = h + d_h;
		var this_type = $("#pen-"+tID+" .type-column-header").html();
		var this_fuzzy_date = $("#pen-"+tID+" .fuzzy-column-header").html();
		if ( (sort_type == "date") && ((tail == false) && (this_fuzzy_date == "Undated")) ) {
			tail = true;
		}
		if ( ((h_new >= H) && (h_meta == true)) || ((sort_type == "type") && (this_type !== label_meta)) || ((tail_meta==true) && (tail==true)) ) {
			label_meta = this_type;
			if (tail == true) {
				tail_meta = false;
			}

			if (c > 0) {
				output += "</div>";
			}
			c++;

			output += "<div id='"+c+"-column-header' class='column-header' style='left: "+(left_spacing+10)+"px;'></div>";
			output += "<div id='column-"+c+"' class='column' style='left: "+left_spacing+"px'>";
			
			left_spacing += t_w;
			h_vec.push(h);
			h = d_h ;
		} else {
			h = h_new;
		}
		if ($(window).width() < 1000) {
			h_meta = false;
		}
		output += "<div class='tile animate' id='"+tID+"' style='"+t_style+"' onclick='javascript: render_tile_overlay(\""+tID+"\");'>"+$(this).html()+"</div>";
	});
	h_vec.push(h);

	$("#tile-pen").hide();
	output += "</div>";
	
	$("#tile-display").html(output); // display is overwrriten, possibly destroying tiles.  can we work around this?

	label_meta = ""; // reset dummy meta var
	for (var i = 1; i <= c; i++) { // generates column headers
		//alert($('.column').height()+" "+h_vec);
		//var off_h = (( column_h - h_vec[i])/2)+"px";
		//$('#column-'+i).css({"padding-top": off_h});
		var header = "";
		if ( (sort_type == "all") || (sort_type == "date") ) {
			var c_t = $("#column-"+i+" .fuzzy-column-header");
		} else {
			var c_t = $("#column-"+i+" .type-column-header");
		}
		c_t.each(function() {
			if ($(this).html().length > 0) {
				header = $(this).html();
				return false;
			}
		});
		if ( ( (header.length > 0) && (header == label_meta) ) && (sort_type == "type") ) {
			$('#column-'+i).css({"border-left": "none", "max-width": ((parseFloat( $('#column-'+i).css("max-width"))+1)+"px")});
			$('#column-'+(i-1)).css( {"border-right": "none", "max-width": ((parseFloat( $('#column-'+(i-1)).css("max-width")) +1)+"px")} );
		}
		if ( (header.length > 0) && (header !== label_meta) ) {
			$("#"+i+"-column-header").html(header);
			label_meta = header;
		}
	}
	var w = String( (c * t_w))+"px";
	$("#tile-display").css("width", w);
	$('#tile-display').show();
	$('#tile-display .tile').click(function() {
		render_tile_overlay($(this).attr("id"));
	}); 
	$('a').click(function(e) {
		e.stopPropagation();
	});
}*/

function render_tile_pen() {	
	var Q = $('#search-input').val();
	if (Q.length > 0) {
		//document.getElementById('search-input-icon').className = "fa fa-fw fa-times-circle expanded";
		$("#search-input-icon").attr("src", "img/delete13.png");
		$("#search-input-icon").addClass("expanded");
		$("#search-input").css("background", "#fff");
	} else {
		//document.getElementById('search-input-icon').className = "fa fa-fw fa-search";
		$("#search-input-icon").attr("src", "img/search4.png");
		$("#search-input-icon").removeClass("expanded");
		$("#search-input").css("background", "");
	}
	$.ajax({
		type: "GET",
		url: "php/render-tile-pen.php",
		data: {query: Q, type: sort_type},
		success: function(data) {
			iframe_clear();
			$("#tile-pen").html(data);
			if (data.length == 0) {
				$("#tile-display").html("<div id='noresults-col' class='column'><div id='noresults-tile' class='tile'><div class='row tile-content' style='text-align: center; font-style: italic;'><img src='img/kenobi.jpg' id='noresults' /><br>These aren't the results you're looking for.</div></div></div>");
				$("#load-overlay").hide();
				$('#noresults-col').fadeIn();
			} else {
				$('#noresults-col').remove();
				$('#tile-display .tiles, #tile-display .column, #tile-display .column-header').hide();
				$("#load-overlay").show();
				$('#tile-display .tiles, #tile-display .column').remove(); 
				$('#tile-pen iframe').each(function() {
					$(this).css({"width": $(this).attr("iwidth"), "height": $(this).attr("iheight")});
				});
				v_scroll = false;
				var imgLoad = imagesLoaded( "#tile-pen" ); // when resources are loaded, render the display
				imgLoad.on( 'always', function( instance ) {
					render_tile_display_new();
					$('a').click(function(e) {
						e.stopPropagation();
					});
				});
			}


		}
	});
}

var overlay_active = false;
function render_tile_overlay(tID) {
	if (overlay_active == true) { // if the overlay is already open, just close it
		return;
	}
	$.ajax({
		type: "GET",
		url: "php/render-tile-overlay.php",
		data: {tID: tID},
		success: function(data) {
			overlay_active = true;
			$("#tile-overlay").html(data);			
			$("#tile-overlay").slideDown();
			$("#overlay-panel").css("border", ("10px solid "+$('#'+tID).css("border-color")));
			$('#tile-display').css("opacity", "0.5");
			$('#tile-overlay iframe').each(function() {
				$(this).css({"width": $(this).attr("iwidth"), "height": $(this).attr("iheight")});
			});
		}
	});
}
function hide_tile_overlay() {
	overlay_active = false;
	$("#tile-overlay").fadeOut();
	$('#tile-display').css("opacity", "1");
}

// these callbacks manage the passive responsiveness
$(window).ready(function() {
	if ( $(window).width() < 700 ) {
		v_scroll = true;
	}
    c_search(); // this will center the user name
	render_tile_pen();
});
$(window).on({
  orientationchange: function(e) {
	render_tile_display_new();
	if ( $('#search-input').is(":focus") || ($('#search-input').val().length >0)) {
		e_search();
	}
  }/*, resize: function(e) {
	render_tile_display_new();
	if ( $('#search-input').is(":focus") || ($('#search-input').val().length >0)) {
		e_search();
	} else {
		c_search();
	}
  }/*, scroll: function(e) {
    iframe_set_track();
  }*/
});

$(window).on("debouncedresize", function() {
	render_tile_display_new();
	if ( $('#search-input').is(":focus") || ($('#search-input').val().length >0) || $("#search-input").hasClass("expanded") ) {
		e_search();
	} else {
		c_search();
	}
});
$.event.special.debouncedresize.threshold = 100;

$("#search-input").keyup(function(e) { // search on user keystroke
	render_tile_pen();
});

function e_search() {		// sets the top menu bar to "search" mode
	$("#search-input, #top-menu-name").addClass("expanded");
	var input_l = $("#top-menu").width() - 52;
	if ($(window).width() > 450) {
		input_l -=  $('#top-menu-name').width();
	}
	$('#search-input').css({"width": ((input_l-35)+"px"), "max-width": ((input_l-35)+"px")});
	$('#top-menu-name').css({"left": (input_l+"px"), "color": "#444"});
}
function c_search() {		// sets the top menu bar to closed mode
	var Q = $('#search-input').val();
	if (Q.length == 0) { // only close search if there's nothing in the input
		//$('#top-menu-name').css({"right": ""});
		$("#top-menu-name, #search-input").removeClass("expanded");
		$('#search-input').css({"width": ""});
		$("#top-menu-name").css("left", ((0.5*($("#top-menu").width() - $('#top-menu-name').width()))+"px"));
	}
}
function clear_search() { // this gets called when the search/X icon in the #search-input is clicked
	var Q = $('#search-input').val();
	if (Q.length > 0) {
		$('#search-input').val("");
		$("#search-input-icon").removeClass("expanded");
		$("#search-input-icon").attr("src", "img/search4.png");
		//document.getElementById("search-input-icon").className = "fa fa-fw fa-search";
	} else {
		e_search();
	}
	 render_tile_pen();
	 $("#search-input").focus();
}
$('#search-input, #search-input-icon').hover(function() {
	e_search();
});
$('#search-input').focus(function() {
	e_search();
});
$('#search-input').focusout(function() {
	c_search();
});
$('#search-input, #search-input-icon').mouseleave(function() {
	if (!$('#search-input').is(":focus")) {
		c_search();
	}
});
$('#layout').click(function(e) {
	var element = e.target;
	e.stopPropagation();
	if (overlay_active == true) {
		//e.stopPropagation();
		if ( !( $(element).is('#overlay-panel *, #overlay-panel')) ) {
			hide_tile_overlay();
		}
    }
});
$('#top-menu').click(function(e) {
    element = e.target;
	if ( !$(element).is('#top-menu-btn *, #top-menu-btn')  ) {
		e.stopPropagation();
        e_search();
        $('#search-input').focus();
	}
});
$('#top-menu').hover(function(e) {
    element = e.target;
	if ( !$(element).is('#top-menu-btn *, #top-menu-btn')  ) {
		e.stopPropagation();
        e_search();
        $('#search-input').focus();
	}
});
$('#top-menu').mouseleave(function() {
	if (!$('#search-input').is(":focus")) {
		c_search();
	}
});

var sort_type = "type";
function change_sort_mode(type) {
	switch (sort_type) {
		case "type":
			//document.getElementById("top-menu-btn").className="fa fa-fw fa-calendar";
			$('#top-menu-btn').attr("src", "img/calendar68.png")
			sort_type = "date";
		 	break;
		case "date":
			//document.getElementById("top-menu-btn").className="fa fa-fw fa-columns";
			$('#top-menu-btn').attr("src", "img/3x3.png")
			sort_type = "type";
			break;
	} 
	
	$("#top-menu-dropdown").fadeOut();
	render_tile_pen();
}